#include "stdafx.h"
#include "Editor_Wrapper.h"

using namespace Editor_Wrapper;

CEditor_Wrapper::CEditor_Wrapper()
{
	m_pEditor = new CEditor();

	return;
}

CEditor_Wrapper::~CEditor_Wrapper()
{
	this->!CEditor_Wrapper();

	return;
}

CEditor_Wrapper::!CEditor_Wrapper()
{
	delete m_pEditor;
	m_pEditor = nullptr;

	return;
}

bool CEditor_Wrapper::Init(IntPtr pRenderTargetHandle, UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (!m_pEditor->Init((HWND)(void*)pRenderTargetHandle, iRenderTargetWidth, iRenderTargetHeight))
		return false;

	return true;
}

void CEditor_Wrapper::Frame()
{
	m_pEditor->Frame();

	return;
}

void CEditor_Wrapper::Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if(m_pEditor != nullptr)
		m_pEditor->Resize(iRenderTargetWidth, iRenderTargetHeight);

	return;
}

IntPtr CEditor_Wrapper::WndProc(IntPtr hWnd, int iMsg, IntPtr wParam, IntPtr lParam)
{
	return (IntPtr)m_pEditor->WndProc((HWND)hWnd.ToPointer(), iMsg, (WPARAM)wParam.ToPointer(), (LPARAM)lParam.ToPointer());
}