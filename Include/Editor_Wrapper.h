#pragma once
#ifndef EDITOR_WRAPPER_EDITOR_WRAPPER_H
#define EDITOR_WRAPPER_EDITOR_WRAPPER_H

#include <Editor.h>

using namespace System;

namespace Editor_Wrapper
{
	public ref class CEditor_Wrapper
	{
		public:
			CEditor_Wrapper();
			~CEditor_Wrapper();
			!CEditor_Wrapper();

			bool Init(IntPtr pRenderTargetHandle, UINT iRenderTargetWidth, UINT iRenderTargetHeight);
			void Frame();
			void Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight);
			IntPtr WndProc(IntPtr hWnd, int iMsg, IntPtr wParam, IntPtr lParam);

		private:
			CEditor * m_pEditor;
	};
}

#endif